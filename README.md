My tomcat 8.5 systemd script.

I got the script and the explanation from https://jdebp.eu/FGA/systemd-house-of-horror/tomcat.html.

All credits go to Jonathan de Boyne Pollard (https://jdebp.eu).

USAGE:

The tomcat.default file contains the environment variables and should be copied to /etc/default/tomcat.
